﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine; 

public class PlayerController : MonoBehaviour {
	
	public int playerspeed = 10; 

	public int playerJumpPower = 1250; 
	private float moveX; 
	public bool isGrounded;
	public float distanceToBottomOfPlayer;


	// Update is called once per frame 
	void Update ()
	{
		PlayerMove ();
		PlayerRaycast ();
	}

	void PlayerMove(){
		//CONTROLS 
		//Debug.Log(isGrounded);


		moveX = Input.GetAxis("Horizontal"); 
		if (Input.GetButtonDown ("Jump") && isGrounded == true) 
		{
			Jump();
		}
		//ANIMATIONS 
		if (moveX != 0) {
			GetComponent<Animator> ().SetBool ("Speed", true);
		} else {
			GetComponent<Animator> ().SetBool ("Speed", false);
		}

		//PLAYER DIRECTION
		if (moveX < 0.0f ) 
		{ 
			GetComponent<SpriteRenderer>().flipX = true;
		} 
		else if (moveX > 0.0f )
		{ 
			GetComponent<SpriteRenderer>().flipX = false;
		} 
		//PHYSICS
		gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerspeed, gameObject.GetComponent<Rigidbody2D>().velocity.y); 
		GetComponent<Animator> ().SetBool ("Grounded", isGrounded);
	}



	void Jump() 
	{ 
		//JUMPING CODE
		GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerJumpPower);
		isGrounded = false;


	} 


	void PlayerRaycast ()
	{	//Ray UP
		RaycastHit2D rayUp = Physics2D.Raycast (transform.position, Vector2.up);
		if (rayUp.collider != null && rayUp.distance < distanceToBottomOfPlayer && rayUp.collider.name == "MysteryBox") {
			Destroy (rayUp.collider.gameObject);
		}
		//Ray DOWN
		RaycastHit2D rayDown = Physics2D.Raycast (transform.position, Vector2.down);

		if (rayDown.collider != null && rayDown.distance < distanceToBottomOfPlayer && rayDown.collider.tag == "enemy") {
			GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 1000);
			rayDown.collider.gameObject.GetComponent<Rigidbody2D> ().AddForce (Vector2.right * 200);
			rayDown.collider.gameObject.GetComponent<Rigidbody2D> ().gravityScale = 8;
			rayDown.collider.gameObject.GetComponent<Rigidbody2D> ().freezeRotation = false;
			rayDown.collider.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			rayDown.collider.gameObject.GetComponent<EnemyMove> ().enabled = false;
			//Destroy (hit.collider.gameObject);
		}



        //Debug.Log(rayDown.distance);

		if (rayDown.collider != null && rayDown.distance <= 2.0f && rayDown.collider.tag != "enemy") {
			isGrounded = true;
		}
	}

}
		