﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour {

	private float timeLeft = 300;
	public static int playerScore = 0;
    public static int deaths = 0;
    public GameObject timeLeftUI;
	public GameObject playerScoreUI;
    public GameObject deathCounterUI;
	public AudioClip coinPickup;

	void Start () {
	DataManagement.datamanagement.LoadData();
	}

	void Update () {
		timeLeft -= Time.deltaTime;
		timeLeftUI.gameObject.GetComponent<Text>().text = ("Time Left: " + (int)timeLeft);
		playerScoreUI.gameObject.GetComponent<Text>().text = ("Score: " + playerScore);
        deathCounterUI.gameObject.GetComponent<Text>().text = ("Deaths: " + deaths + "☠");
		if (timeLeft < 0.1f) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
	}
	void OnTriggerEnter2D (Collider2D trig){
		if (trig.gameObject.name == "EndLevel") {
			CountScore ();

		}
		if (trig.gameObject.tag == "Coin") {
			playerScore += 10;
			Destroy (trig.gameObject);
			Debug.Log("collected coin");
			GetComponent<AudioSource>().clip = coinPickup;
			GetComponent<AudioSource>().Play();


		}

        if (trig.gameObject.tag == "DeathZone")
        {
            deaths++;
			IncrementDeaths();
        }

		if (trig.gameObject.tag == "enemy")
		{
			deaths++;
			IncrementDeaths();
		}
	}

	void CountScore() {
		Debug.Log ("Data says high score is currently " + DataManagement.datamanagement.highScore);
		playerScore = playerScore + (int)(timeLeft * 10);
		DataManagement.datamanagement.highScore = playerScore;
		DataManagement.datamanagement.SaveData ();
		Debug.Log ("Now that we have added the score to DataManagement, Data says high score is currently " + DataManagement.datamanagement.highScore);
	}
	
	void IncrementDeaths()
	{
		DataManagement.datamanagement.noOfDeaths = deaths;
		DataManagement.datamanagement.SaveData();
	}

}

