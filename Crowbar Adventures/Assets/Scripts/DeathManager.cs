﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathManager : MonoBehaviour
{
    

    public int deaths = 0;

    public Text deathText;

         

    void IncreaseDeaths()
    {
        deaths += 1;
        deathText.text = deaths.ToString();
    }
}
