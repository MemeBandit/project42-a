﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {

	// private DeathManager dm;
	public GameObject Player;

    [SerializeField]
    private GameObject gameOverUI;

    void Update () {
		if (gameObject.transform.position.y < -50) {
			Die ();
			//PlayerScore.deaths++;
		}

 	}


	void Die ()
	{

        Debug.Log("GAME OVER");
        gameOverUI.SetActive(true);
		//PlayerScore.deaths++;
		

	}

	void OnTriggerEnter2D (Collider2D trig){
		if (trig.gameObject.tag == "DeathZone") {
			Die ();
			Destroy(GameObject.Find("Player"));
			//dm = GameObject.FindObjectOfType<DeathManager>();
		}
		if (trig.gameObject.name == "EndLevel")
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
		}

	}
}
